
(defrule join0
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samUha_meM_SAmila_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " join.clp	join0  "  ?id "  " ?id1 "  samUha_meM_SAmila_ho  )" crlf))
)

;They were going for a movie && i joined on.
;ve sinemA jA rahe We Ora mEM BI unake samUha meM SAmila ho gayA
(defrule join1
(declare (salience 4900))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 surakRAbala_kA_saxasya_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " join.clp	join1  "  ?id "  " ?id1 "  surakRAbala_kA_saxasya_ho  )" crlf))
)

;He joined up in 1950.
;1950 meM vaha surakRAbala kA saxasya banA
(defrule join2
(declare (salience 4800))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join2   "  ?id "  mila )" crlf))
)

;default_sense && category=verb	saMyukwa_kara	0
;"join","VT","1.saMyukwa_karanA"
;I have joined the two wires.
;--"2.milanA"
;Lets join together in our effort to bring peace in the society.
;--"3.SAmila_honA"
;I joined the Navy in 1998.
;
;

;Added by Prachi Rathore[22-11-13]
;I don't have time for a drink now, but I'll join you later. 
;मेरे पास अभी पेय या शरबत के लिए समय नहीं है, परन्तु मैं बाद में आपका साथ दूँगा .. 
(defrule join3
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join3   "  ?id "  sAWa_xe )" crlf))
)

;Added by Prachi Rathore[22-11-13]
; It's a great club, why don't you join? 
;यह क्लब बहुत अच्छा है ,तुम क्यों नहीं इसके सदस्य बन जाते .
(defrule join4
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saxasya_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join4   "  ?id "  saxasya_bana )" crlf))
)

;Added by Prachi Rathore[22-11-13]
; If you join the dots on the paper, you'll get a picture.
;अगर तुम पेपर पर के  दोनों बिंदुओं को जोड़ दो तो ,तुम्हें एक तस्बीर दिखेगी . 
(defrule join5
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join5   "  ?id "  joda )" crlf))
)

;Added by Prachi Rathore[22-11-13]
;I felt so unfit after Christmas that I decided to join a gym.
;क्रिसमस के बाद मैंने जिम की सदस्यता लेने का निश्चय किया
(defrule join6
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 gym)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saxasya_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join6   "  ?id "  saxasya_bana )" crlf))
)

;Added by Prachi Rathore[22-11-13]
;A long suspension bridge joins the two islands.
;एक लम्बा पुल दोनों द्वीप को जोड़ता है.
(defrule join7
(declare (salience 4900))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(and(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?id2))
(and(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))(not(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join7   "  ?id "  joda )" crlf))
)

;Added by Prachi Rathore[22-11-13]
;If you've come to buy tickets for tonight's performance, please join the UK queue
;अगर तुम आज की रात का प्रदर्शन (नाटक ) देखने आए हो तो कतार के अंत में शामिल होना होगा .
(defrule join8
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 rank|queue)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAmila_ho))
(assert (kriyA_id-object_viBakwi ?id me))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join8   "  ?id "  SAmila_ho )" crlf))
)


;;@@@   ---Added by Prachi Rathore
; She was prodded into joining the team. [cambridge]
;उसे टीम में सम्मिलित होने के लिये उकसाया गया था .
(defrule join9
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammiliwa_ho))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	join9   "  ?id "  sammiliwa_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*wsd_dir* "  join.clp     join9   "  ?id "  meM )" crlf))
)
