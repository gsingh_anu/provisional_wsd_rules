;##############################################################################
;#  Copyright (C) 2013-2014 Anita Chaturvedi (anita@iiit.ac.in)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@Added by Anita-28.11.2013
;He is a man of good reading. [old read.clp sentence from hinkhoj dictionary]
;वह एक सुपठित आदमी है ।
(defrule read0
(declare (salience 4000))
(id-root ?id reading)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 man|woman|boy|girl)
(viSeRya-viSeRaNa  ?id ?)
(viSeRya-of_saMbanXI  ?id1 ?id)
;(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id supaTiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  read.clp  	read0   "  ?id "  supaTiwa)" crlf))
)
;@@@Added by Anita-28.11.2013 
;He has come for water meter reading. [old read.clp sentence]
;वह पानी के मीटर की रीडिंग लेने के लिए आया है ।
(defrule read1
(declare (salience 4800))
(id-root ?id reading)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 meter)
(kriyA-for_saMbanXI  ? ?id)
;(samAsa  ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id rIdiMga_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  read.clp  	read1   "  ?id "  rIdiMga_le)" crlf))
)
;@@@Added by Anita-28.11.2013 
;Reading and tennis are my favourite pastimes. [cambridge dictionary]
;अध्ययन और टेनिस मेरा मनपसन्द मनोविनोद हैं ।
;The society often arranges poetry readings and musical evenings. [cambridge dictionary]
;समाज अक्सर कविता अध्ययन संगीतात्मक शामों का प्रबन्ध करता है ।
(defrule read2
(declare (salience 4950))
(id-root ?id reading)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ? ?id ?)
;(kriyA-for_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aXyayana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  read.clp  	read2   "  ?id "  aXyayana)" crlf))
)
;;@@@Added by Anita-28.11.2013 
;These books are required reading for students of architecture. [cambridge dictionary]
;वास्तुकला के विद्यार्थियों के लिए इन पुस्तकों पठन आवश्यक है ।
(defrule read3
(declare (salience 5000))
(id-root ?id reading)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 student)
(kriyA-for_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id paTana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  read.clp  	read3   "  ?id "  paTana)" crlf))
)
