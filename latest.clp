;##############################################################################
;#  Copyright (C) 2002-2005 Nandini Upasani (nandini.upasani@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;############################################################################
;(28-11-13)
;The latest unemployment figures shows drastic change in the graph.
(defrule latest1
(declare (salience 150))
(id-word ?id latest)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
;(or(kriyA-object  ? ?id1)(kriyA-subject ? ?id1))
(id-word ?id1 news|figures)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id wAjzA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  latest.clp  	latest1   "  ?id "  wAjzA )" crlf))
)

;She likes latest fashion.
(defrule latest0
(declare (salience 100))
(id-word ?id latest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id navInawama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  latest.clp  	latest0   "  ?id "  navInawama )" crlf))
)

