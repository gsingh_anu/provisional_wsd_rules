
;############################################################################
;#  Copyright (C) 2013-2014 Krithika (krithika DOT ns @ gmail DOT com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;############################################################################

; @@@ Added by Krithika 10/12/2013
;  The lion gobbled down its prey. [Hinkhoj]
; siMha ne usake SikAra ko nigalA.

; He gobbled up all the news. [freedictionary]
; usane saBI samAcAra ko nigalA.

(defrule gobble_down1
(declare (salience 100))
(id-root ?id gobble)
(id-root ?id1 down|up)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nigala))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  gobble.clp	gobble_down1  "  ?id "  " ?id1 "  nigala )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  gobble.clp  gobble0   "  ?id " ko )" crlf)
)

;____________________________________________________
; @@@ Added by Krithika 10/12/2013
; We could hear the turkeys gobbling in the farmyard. [freedictionary]
; hama PArma_ke hAwe meM GuraGurAwe_hue pIrU ko suna sake.

(defrule gobble1
(declare (salience 100))
(id-root ?id gobble)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id) (kriyA-subject  ?id ?))


(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GuraGurA))
;(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gobble.clp 	gobble1    "  ?id " GuraGurA )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  gobble.clp  gobble1   "  ?id " ko )" crlf)
)

;################### Default rules ####################
; @@@ Added by Krithika 10/12/2013
; She gobbled her dinner. [Cambridge]
; usane usake rAwa_ke Bojana ko nigalA.
	
(defrule gobble0
;(declare (salience 10))
(id-root ?id gobble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>	
(retract ?mng)
(assert (id-wsd_root_mng ?id nigala))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gobble.clp 	gobble0    "  ?id " nigala )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  gobble.clp  gobble0   "  ?id " ko )" crlf)

)
;################### Additional Examples ####################

; He is very good at gobbling while imitating the sound of birds.
; Don't gobble your food like that!
; She gobbled her dinner.
; When a turkey gobbles, it makes a noise in its throat.
; He gobbled all the beef stew.
; You'll be sick if you keep gobbling your meals like that.
; Hotel costs gobbled up most of their holiday budget.
; It will then become so inflated that it will swallow the closer planets Venus and Mercury and also gobble up the Earth!
; They gobbled down all the sandwiches.
; The children gobbled down most of the birthday cake.
; When Do Turkeys Gobble The Most?
; When a turkey gobbles, it makes a noise in its throat.
