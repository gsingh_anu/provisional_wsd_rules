
(defrule venture0
(declare (salience 5000))
(id-root ?id venture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAhasika_kArya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  venture.clp 	venture0   "  ?id "  sAhasika_kArya )" crlf))
)

;"venture","N","1.sAhasika_kArya"
;To climb the peak during the winter months was a tough venture.
;
(defrule venture1
(declare (salience 4900))
(id-root ?id venture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAhasa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  venture.clp 	venture1   "  ?id "  sAhasa_kara )" crlf))
)

;"venture","VTI","1.sAhasa_karanA"
;The students ventured into the high-sea
;He ventured his opinion in the meeting
;

;@@@ Added by Prachi Rathore[03-12-13]
;They ventured nervously into the water.[OALD]
;उन्होंने  घबराते हुए पानी के अंदर जाने का साहस किया . 
(defrule venture2
(declare (salience 5000))
(id-root ?id venture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAne_kA_sAhasa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  venture.clp 	venture2   "  ?id "  jAne_kA_sAhasa_kara )" crlf))
)

;;@@@   ---Added by Prachi Rathore
;As soon as it was light the chowkidar lit a fire, but the cold prevented the children from venturing outdoors.[gyannidhi]
;सुबह हुई और डाक-बंगले के चौकीदार ने आग जला दी,परन्तु ठण्ड ने गाँव का  बाहर जाने से बच्चों को रोका . 
(defrule venture3
(declare (salience 5000))
(id-root ?id venture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ? ?id)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  venture.clp 	venture3   "  ?id "  jA )" crlf))
)


;;@@@   ---Added by Prachi Rathore[12-12-13]
;Our workers are so frightened that they refuse to [venture out] at night.[gyannidhi]
;हमारे मजदूर तो इतने डर गए थे कि वे रात में अपने घरों से बाहर निकलने का जोखिम नहीं उठाते थे।
(defrule venture4
(declare (salience 5000))
(id-root ?id venture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAhara_nikalane_kA_joKima_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " venture.clp	venture4  "  ?id "  " ?id1 "  bAhara_nikalane_kA_joKima_le  )" crlf)))
